#[macro_use]
extern crate log;

use color_eyre::eyre::Result;
use input::{event::DeviceEvent, Event, Libinput, LibinputInterface};
use libc::{O_RDONLY, O_RDWR, O_WRONLY};
use std::os::unix::{
    fs::OpenOptionsExt,
    io::{FromRawFd, IntoRawFd, RawFd},
};
use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};
use std::{
    fs::{File, OpenOptions},
    io::Write,
};
use udev::Device;

struct Interface;

impl LibinputInterface for Interface {
    fn open_restricted(&mut self, path: &Path, flags: i32) -> Result<RawFd, i32> {
        OpenOptions::new()
            .custom_flags(flags)
            .read((flags & O_RDONLY != 0) | (flags & O_RDWR != 0))
            .write((flags & O_WRONLY != 0) | (flags & O_RDWR != 0))
            .open(path)
            .map(|file| file.into_raw_fd())
            .map_err(|err| err.raw_os_error().unwrap())
    }
    fn close_restricted(&mut self, fd: RawFd) {
        unsafe {
            File::from_raw_fd(fd);
        }
    }
}

fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    if let Some((tablet_device, touch_device)) = find_touchscreen_device() {
        let mut input = Libinput::new_from_path(Interface);
        input.path_add_device(tablet_device.devnode().unwrap().to_str().unwrap());

        let mut last_deactivation = None;
        loop {
            input.dispatch().unwrap();
            for event in &mut input {
                if let Event::Tablet(_event) = event {
                    trace!("Disabling touch device {:?}", touch_device.sysname());
                    unauthorized_device(touch_device.syspath());
                    last_deactivation = Some(Instant::now());
                }
            }

            std::thread::sleep(Duration::from_millis(5));

            last_deactivation = match last_deactivation.take() {
                None => None,
                Some(last_deactivation) => {
                    let now = Instant::now();
                    if (now - last_deactivation) > Duration::from_secs(2) {
                        trace!("Enabling touch device {:?}", touch_device.sysname());
                        // This is a hack: authorizing the device again did not work. However,
                        // unauthorizing the parent device authorizes the device after delay.
                        unauthorized_device(touch_device.parent().unwrap().syspath());
                        None
                    } else {
                        Some(last_deactivation)
                    }
                }
            }
        }
    }

    Ok(())
}

fn find_touchscreen_device() -> Option<(Device, Device)> {
    use input::event::EventTrait;

    let mut input = Libinput::new_with_udev(Interface);
    input.udev_assign_seat("seat0").unwrap();
    input.dispatch().unwrap();

    let mut touch_device = None;
    let mut tablet_device = None;

    for event in &mut input {
        if let Event::Device(DeviceEvent::Added(event)) = event {
            let device = event.device();

            trace!("Found device {}", device.name());

            if device.has_capability(input::DeviceCapability::TabletTool) {
                debug!("Found tablet device: {}", device.name());
                tablet_device = unsafe { device.udev_device() };
            }

            if device.has_capability(input::DeviceCapability::Touch) {
                debug!("Found touch device: {}", device.name());
                touch_device = find_authorizable_root(unsafe { device.udev_device() });
            }
        }
    }

    match (tablet_device, touch_device) {
        (Some(tablet_device), Some(touch_device)) => {
            info!(
                "Tracking tablet {:?} to deactive touch {:?}",
                tablet_device.sysname(),
                touch_device.sysname()
            );
            Some((tablet_device, touch_device))
        }
        _ => None,
    }
}

fn find_authorizable_root(device: Option<Device>) -> Option<Device> {
    let device = device?;

    let mut path = device.syspath().to_path_buf();
    path.push("authorized");
    if path.exists() {
        return Some(device);
    }

    find_authorizable_root(device.parent())
}

fn unauthorized_device<P>(device_path: P)
where
    P: Into<PathBuf>,
{
    let mut path = device_path.into();
    path.push("authorized");

    let mut f = match OpenOptions::new().write(true).open(path.clone()) {
        Ok(f) => f,
        Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
            debug!("Cannot open {:?}. Trying again later", path);
            return;
        }
        Err(err) => {
            error!(
                "Cannot open authorized udev attribute file ({:?}): {}",
                path, err
            );
            return;
        }
    };

    if let Err(err) = f.write_all("0".as_bytes()) {
        eprintln!("Cannot unauthorize device ({:?}): {}", path, err);
    }
}
