# Yoga Wacom Proximity

```
  ____                                _           _
 |  _ \  ___ _ __  _ __ ___  ___ __ _| |_ ___  __| |
 | | | |/ _ \ '_ \| '__/ _ \/ __/ _` | __/ _ \/ _` |
 | |_| |  __/ |_) | | |  __/ (_| (_| | ||  __/ (_| |
 |____/ \___| .__/|_|  \___|\___\__,_|\__\___|\__,_|
            |_|
```

This is a small utility tool that deactivates the touchscreen of the [Thinkpad Yoga 260](https://en.wikipedia.org/wiki/ThinkPad_Yoga#Yoga_260)
when using [lipinput](https://www.freedesktop.org/wiki/Software/libinput/) on an Wayland session.

This tool has been inspired by [Thinkpad Yoga Scripts](https://github.com/admiralakber/thinkpad-yoga-scripts). Due to the fact that Wayland does not support disabling touchscreen through
`xinput` anymore, this tool has been written.

# Install and Usage

This runs currently on Arch Linux. Run following commands to install and enable the package:

```bash
# Get the sources
git clone https://gitlab.com/schrieveslaach/yoga-wacom-proximity.git
cd yoga-wacom-proximity

# Build the package
makepkg

# Install and enable
sudo pacman -U yoga-wacom-proximity-*-x86_64.pkg.tar.zst
sudo systemctl enable yoga-wacom-proximity
sudo systemctl start yoga-wacom-proximity
```

# Demo

## Before

When using the the stylus without deactivating the touchscreen, the touchscreen invokes random actions
due to the hand lying on the display:

![Drawing does not work](does-not-work.gif)

## After

With the service enabled, the touchscreen gets deactivated:

![Drawing works](works.gif)
